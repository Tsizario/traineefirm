﻿using TraineeFirm.Abstractions;

namespace TraineeFirm.Models;

public class Firm
{
    public List<Employee> ListOfALlEmployees;

    public Firm(List<Employee> listOfALlEmployees)
    {
        ListOfALlEmployees = listOfALlEmployees;
    }

    public static Firm operator +(Firm firm, Employee employee)
    {
        firm.ListOfALlEmployees.Add(employee);

        return firm;
    }
    
    public static Firm operator -(Firm firm, Employee employee)
    {
        firm.ListOfALlEmployees.Remove(employee);

        return firm;
    }

    public List<T> GetAllEmployees<T>()
    {
        return ListOfALlEmployees.OfType<T>().ToList();
    }

    public int GetCountOfType<T>()
    {
        return ListOfALlEmployees.Count(x => x is T);
    }
}