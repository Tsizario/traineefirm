﻿using TraineeFirm.Abstractions;

namespace TraineeFirm.Models;

public class Brigadier : Employee
{
    public Brigadier(string firstName, string lastName, string middleName, double experience)
        : base(firstName, lastName, middleName, experience)
    {
    }

    public override void DoWork()
    {
        PurchaseMaterials();
    }

    public void PurchaseMaterials()
    {
    }

    public void CheckWorkers()
    {
        Console.WriteLine("The workers has been checked");
    }
}