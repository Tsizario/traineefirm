﻿using TraineeFirm.Abstractions;

namespace TraineeFirm.Models;

public class Manager : Employee
{
    public Manager(string firstName, string lastName, string middleName, double experience) 
        : base(firstName, lastName, middleName, experience)
    {
    }

    public override void DoWork()
    {
        CollectOrders();
    }

    public void CollectOrders()
    {
    }

    public void GiveTask()
    {
        Console.WriteLine("The task has been given");
    }
}