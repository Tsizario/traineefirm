﻿using TraineeFirm.Abstractions;

namespace TraineeFirm.Models;

public class Worker : Employee
{
    public Worker(string firstName, string lastName, string middleName, double experience) 
        : base(firstName, lastName, middleName, experience)
    {
    }

    public override void DoWork()
    {
        ReleaseProduction();    
    }
    
    public void ReleaseProduction()
    {
    }
}