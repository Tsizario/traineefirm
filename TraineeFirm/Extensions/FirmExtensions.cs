﻿using System.Text;
using TraineeFirm.Abstractions;
using TraineeFirm.Models;

namespace TraineeFirm.Extensions;

public static class FirmExtensions
{
    private const string OutputFormat = "|{0, -10}|{1, -15}|{2, -15}|{3, -15}|{4, -10}|";
    
    public static bool IsEmployeeOfFirm(this Firm firm, Employee employee)
    {
        return firm.ListOfALlEmployees.Contains(employee);
    }

    public static void PrintAllEmployees(this Firm firm)
    {
        StringBuilder stringBuilder = new StringBuilder();
        
        string outputColumnsFormat = 
            string.Format(
                OutputFormat,
                "Position", "Lastname", "Name", "Middlename", "Experience");

        Console.Write(
            stringBuilder
                .AppendLine("=".PadRight(93, '='))
                .AppendLine(outputColumnsFormat)
                .AppendLine("=".PadRight(93, '=')));

        foreach (var employee in firm.ListOfALlEmployees)
        {
            Console.WriteLine(
                OutputFormat, 
                employee.GetType().Name, employee.LastName, employee.FirstName, employee.MiddleName, employee.Experience);
        }
    }
}