﻿using TraineeFirm.Abstractions;
using TraineeFirm.Extensions;
using TraineeFirm.Models;

namespace TraineeFirm;

class Program
{
    static void Main(string[] args)
    {
        Firm firm = new Firm(
            new List<Employee>
            {
                new Brigadier(
                    "Alexey", "Panevin", "Igorovich", 4.5),
                new Manager(
                    "Oleksandr", "Tsisarenko", "Igorovich", 1),
                new Worker(
                    "Alexey", "Dediukin", "Sergiyovich", 2.1)
            });

        var worker2 =
            new Worker("Oleksandr", "Svyrydov", "Sergiyovich", 2.5);
        var worker3 =
            new Worker("Peter", "Hohol", "Mazepovich", 2.5);

        firm += worker2;
        firm += worker3;
        firm -= worker3;
        Console.WriteLine(firm.GetAllEmployees<Brigadier>());
        Console.WriteLine(firm.GetCountOfType<Worker>());
        firm.PrintAllEmployees();
    }
}
