﻿namespace TraineeFirm.Abstractions;

public abstract class Employee
{
    public double Experience { get; set; }
    
    public string LastName { get; set; }
    
    public string FirstName { get; set; }
    
    public string MiddleName { get; set; }

    public Employee(string firstName, string lastName, 
        string middleName, double experience)
    {
        FirstName = firstName;
        LastName = lastName;
        MiddleName = middleName;
        Experience = experience;
    }

    public abstract void DoWork();
}